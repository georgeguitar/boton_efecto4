# Proyecto Final del Módulo de Gestión de Paquetes

## Autor

Navarro Arias Juan Dirceu

## Resumen

Componente que se puede utilizar para añadir un efecto de desvanecimiento a un botón html mediante CSS. En la versión beta se puede hacer aparecer un carrito de compras en el botón.

El gestor de paquetes en NPM, nombre del paquete: "boton_efecto"

Disponible en:
 
* Paquete de [npmjs](https://www.npmjs.com/package/boton_efecto4/ "boton_efecto")
* Repositorio en [gitlab](https://gitlab.com/georgeguitar/boton_efecto4.git/ "código boton_efecto")

## Modo de uso

Para utilizar el paquete, se debe instalar el paquete con:

Versión estable:
``` $ npm install boton_efecto4@1.1.1```

Versión beta:
``` $ npm install boton_efecto4@1.1.1-beta.0```

Una vez instalado el paquete, se debe incluir el archivo "button.css"  en el código html.
```
<head>
<link href="node_modules/boton_efecto4/button.css" type="text/css" rel="stylesheet" />
</head>
```
El paquete descargado tienen dos archivos html para hacer la prueba inmediata de la funcionalidad:

* ``` ejemplo-efecto_carrito_de_compras.html ```
* ``` ejemplo-efecto_desvanecimiento.html ```

## Botón con efecto de desvanecimiento.

![ejemplo](screenshot.png)

Para utilizar el efecto, se tiene que incluir el estilo "button".

```
<button type="button" onclick="alert('&iexcl;Opci&oacute;n 1!')" class="button">&iexcl;Opci&oacute;n 1!</button>
```

Ejemplo:
```
<!DOCTYPE html>
<html>

<head>
	<link href="node_modules/boton_efecto4/button.css" type="text/css" rel="stylesheet" />
</head>

<body>
	<h1>Bot&oacute;n con efectos</h1>
	<p>Autor: Juan Navarro</p>

	<button type="button" onclick="alert('&iexcl;Opci&oacute;n 1!')" class="button">&iexcl;Opci&oacute;n 1!</button>

</body>
</html>
```
Preview:
![ejemplo](screenshot1.png)

## Versión Beta

![ejemplo](screenshot_beta.png)

La versión beta tiene un efecto de carrito de compra en el botón.
Para utilizar el efecto, se tiene que incluir el estilo "button1".

```
<button type="button" onclick="alert('&iexcl;Opci&oacute;n 2!')" class="button1">&iexcl;Opci&oacute;n 2!</button>
```

Ejemplo:
```
<!DOCTYPE html>
<html>

<head>
	<link href="node_modules/boton_efecto4/button.css" type="text/css" rel="stylesheet" />
</head>

<body>
	<h1>Bot&oacute;n con efectos</h1>
	<p>Autor: Juan Navarro</p>

	<button type="button" onclick="alert('&iexcl;Opci&oacute;n 2!')" class="button1">&iexcl;Opci&oacute;n 2!</button>

</body>
</html>
```


Preview:
![ejemplo](screenshot_beta1.png)

